#!/bin/bash

#github: return5
#licene: MIT License


#script to write, compile, and run a hello world program in C

{
	printf "%s\n" '#include<stdio.h> //puts'
	printf "\n int main(void) {\n"
	printf "\t%s\n" 'puts("Hello,World");'
	printf "\treturn 0; \n }"
} > "helloworld.c"  #writes program to file 'helloworld.c'

#compiles it to file named 'helloworld'
gcc helloworld.c -ohelloworld

#run it
./helloworld



